package com.charlym.segproyectos.tickets;

import javax.persistence.*;

@Entity(name  = "tickets")
@NamedNativeQuery(name="TicketModel.findAllIn",
        query="select t.id, t.nombre, ifnull(t.descripcion, \"\") as descripcion, pr.nombre as proyecto, ifnull(t.estimacion, 0) as estimacion, " +
                "  tt.descripcion as tipo, t.reportado_por, concat(pe.nombre, ' ', pe.apellido_pat, ' ', pe.apellido_mat) as responsable, " +
                "  et.descripcion as estatus, ifnull(t.observaciones, \"\") as observaciones " +
                "from tickets t, proyectos pr , cat_tipo_ticket tt, personas pe, cat_estatus_ticket et  " +
                "where t.idproyecto=pr.id " +
                "  and t.tipo=tt.id " +
                "  and t.responsable=pe.id " +
                "  and t.estatus=et.id",
        resultClass= TicketModel.class)

public class TicketModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;
    private String nombre;
    private String descripcion;
    private String proyecto;
    private String estimacion;
    private String tipo;
    private String reportado_por;
    private String responsable;
    private String estatus;
    private String observaciones;

}
