package com.charlym.segproyectos.tickets;

import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface TicketRepository extends CrudRepository<TicketModel,Integer> {
    List<TicketModel> findAllIn();
    /*List<TicketModel> findByTipo(int tipo);
    List<TicketModel> findByEstatus(int estatus);
    List<TicketModel> findByResponsable(int responsable);
    List<TicketModel> findByProyecto(int proyecto);*/
}
