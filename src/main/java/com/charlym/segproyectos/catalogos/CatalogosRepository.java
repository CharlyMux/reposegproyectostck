package com.charlym.segproyectos.catalogos;

import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface CatalogosRepository extends CrudRepository<CatalogoModel,Integer> {
        List<CatalogoModel> findAllIn();
        List<CatalogoModel> findByTipo(int tipo);
        }

