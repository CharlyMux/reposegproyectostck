package com.charlym.segproyectos.catalogos;

import javax.persistence.*;

@Entity(name  = "catalogos")
@NamedNativeQuery(name="CatalogoModel.findAllIn",
        query="select * from ( " +
                "select id, descripcion, 1 as tipo from cat_tipo_contrato " +
                "union " +
                "select id, descripcion, 2 as tipo from cat_tipo_proyecto " +
                "union " +
                "select id, descripcion, 3 as tipo from cat_tipo_ticket " +
                "union " +
                "select id, descripcion, 4 as tipo from cat_perfil " +
                "union " +
                "select id, descripcion, 5 as tipo from cat_estatus_proyecto " +
                "union " +
                "select id, descripcion, 6 as tipo from cat_estatus_ticket " +
                ") catalogos2",
        resultClass= CatalogoModel.class)
@NamedNativeQuery(name="CatalogoModel.findByTipo",
        query="select * from ( " +
                "select id, descripcion, 1 as tipo from cat_tipo_contrato " +
                "union " +
                "select id, descripcion, 2 as tipo from cat_tipo_proyecto " +
                "union " +
                "select id, descripcion, 3 as tipo from cat_tipo_ticket " +
                "union " +
                "select id, descripcion, 4 as tipo from cat_perfil " +
                "union " +
                "select id, descripcion, 5 as tipo from cat_estatus_proyecto " +
                "union " +
                "select id, descripcion, 6 as tipo from cat_estatus_ticket " +
                ") catalogos where tipo =?1",
        resultClass= CatalogoModel.class)

public class CatalogoModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String descripcion;
    private int tipo;

    public CatalogoModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "CatalogoModel{" +
                "id=" + id +
                ", descripcion='" + descripcion + '\'' +
                ", tipo=" + tipo +
                '}';
    }
}
