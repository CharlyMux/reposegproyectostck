package com.charlym.segproyectos.controller;

import com.charlym.segproyectos.proyectos.ProyectoModel;
import com.charlym.segproyectos.proyectos.ProyectosRepository;
import com.charlym.segproyectos.catalogos.CatalogoModel;
import com.charlym.segproyectos.catalogos.CatalogosRepository;
import com.charlym.segproyectos.personas.PersonaModel;
import com.charlym.segproyectos.personas.PersonasRepository;
import com.charlym.segproyectos.tickets.TicketModel;
import com.charlym.segproyectos.tickets.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/apiproyseg/v1")
public class SegProyectosController {
    //-----------Personas
    @Autowired
    PersonasRepository personasRepository;

    @GetMapping("/personas")
    public Iterable<PersonaModel> getPersonas(){
            return personasRepository.findAll();
    }

    @GetMapping("/personas/empresa")
    public Iterable<PersonaModel> getPersonasbyEmpresa(@RequestParam(defaultValue = "") String empresa){
        if(empresa==""){
            return personasRepository.findAll();
        } else{
            return personasRepository.findByEmpresa(empresa);
        }
    }

    @GetMapping("/personas/perfil")
    public Iterable<PersonaModel> getPersonasbyPerfil(@RequestParam(defaultValue = "0") int perfil){
        if(perfil==0){
            return personasRepository.findAll();
        }else{
            return personasRepository.findByPerfil(perfil);
        }
    }

    @GetMapping("/personas/fecha")
    public Iterable<PersonaModel> getPersonasbyFechaIngreso(@RequestParam(defaultValue = "") String fechaIngreso){
        if(fechaIngreso==""){
            return personasRepository.findAll();
        }else{
            return personasRepository.findByFechaIngreso(fechaIngreso);
        }
    }

    @GetMapping("/personas/tipoc")
    public Iterable<PersonaModel> getPersonasbyTipoContrato(@RequestParam(defaultValue = "0") int tipoContrato){
        if(tipoContrato==0){
            return personasRepository.findAll();
        }else{
            return personasRepository.findByTipoContrato(tipoContrato);
        }
    }

    //-----------Catalogos
    @Autowired
    CatalogosRepository catalogosRepository;

    @GetMapping("/catalogos")
    public Iterable<CatalogoModel> getCatalogos(@RequestParam(defaultValue = "0") int tipo){
        if(tipo==0){
            return catalogosRepository.findAllIn();
        }else{
            return catalogosRepository.findByTipo(tipo);
        }
    }

    //-----------Proyectos
    @Autowired
    ProyectosRepository proyectosRepository;

    @GetMapping("/proyectos")
    public Iterable<ProyectoModel> getProyectos(){
        return proyectosRepository.findAllIn();
    }

    @GetMapping("/proyectos/estatus")
    public Iterable<ProyectoModel> getProyectosByEstatus(@RequestParam(defaultValue = "0") int estatus){
        if(estatus==0){
            return proyectosRepository.findAll();
        }else{
            return proyectosRepository.findByEstatus(estatus);
        }
    }

    @GetMapping("/proyectos/ramo")
    public Iterable<ProyectoModel> getProyectosByRamo(@RequestParam(defaultValue = "0") String ramo){
        if(ramo=="0"){
            return proyectosRepository.findAll();
        }else{
            return proyectosRepository.findByRamo(ramo);
        }
    }

    //-----------Tickets
    @Autowired
    TicketRepository ticketRepository;

    @GetMapping("/tickets")
    public Iterable<TicketModel> getTickets(){
        return ticketRepository.findAllIn();
    }

}
