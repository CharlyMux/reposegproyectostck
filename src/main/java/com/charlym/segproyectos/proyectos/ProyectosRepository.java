package com.charlym.segproyectos.proyectos;

import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface ProyectosRepository extends CrudRepository<ProyectoModel,Integer> {
    List<ProyectoModel> findAllIn();
    List<ProyectoModel> findByEstatus(int estatus);
    List<ProyectoModel> findByRamo(String estatus);
}


