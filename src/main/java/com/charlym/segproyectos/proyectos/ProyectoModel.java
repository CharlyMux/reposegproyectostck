package com.charlym.segproyectos.proyectos;

import javax.persistence.*;

@Entity(name  = "proyectos")
@NamedNativeQuery(name="ProyectoModel.findAllIn",
        query="select " +
                "p.id, p.nombre, ifnull(p.descripcion, \"\") as descripcion, ifnull(p.sdatool, \"\") as sdatool, ifnull(p.ramo, \"\") as ramo, tp.descripcion as tipo, ep.descripcion as estatus  " +
                "from  " +
                "proyectos p, cat_tipo_proyecto tp, cat_estatus_proyecto ep " +
                "where " +
                "p.tipo = tp.id " +
                "and p.estatus = ep.id ",
        resultClass= ProyectoModel.class)
@NamedNativeQuery(name="ProyectoModel.findByEstatus",
        query="select " +
                "p.id, p.nombre, ifnull(p.descripcion, \"\") as descripcion, ifnull(p.sdatool, \"\") as sdatool, ifnull(p.ramo, \"\") as ramo, tp.descripcion as tipo, ep.descripcion as estatus  " +
                "from  " +
                "proyectos p, cat_tipo_proyecto tp, cat_estatus_proyecto ep " +
                "where " +
                "p.tipo = tp.id " +
                "and p.estatus = ep.id " +
                "and estatus = ?1",
        resultClass= ProyectoModel.class)
@NamedNativeQuery(name="ProyectoModel.findByRamo",
        query="select " +
                "p.id, p.nombre, ifnull(p.descripcion, \"\") as descripcion, ifnull(p.sdatool, \"\") as sdatool, ifnull(p.ramo, \"\") as ramo, tp.descripcion as tipo, ep.descripcion as estatus " +
                "from  " +
                "proyectos p, cat_tipo_proyecto tp, cat_estatus_proyecto ep " +
                "where " +
                "p.tipo = tp.id " +
                "and p.estatus = ep.id " +
                "and ramo = ?1",
        resultClass= ProyectoModel.class)

public class ProyectoModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nombre;
    private String descripcion;
    private String sdatool;
    private String ramo;
    private String tipo;
    private String estatus;

    public ProyectoModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSdatool() {
        return sdatool;
    }

    public void setSdatool(String sdatool) {
        this.sdatool = sdatool;
    }

    public String getRamo() {
        return ramo;
    }

    public void setRamo(String ramo) {
        this.ramo = ramo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    @Override
    public String toString() {
        return "ProyectoModel{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", sdatool='" + sdatool + '\'' +
                ", ramo='" + ramo + '\'' +
                ", tipo='" + tipo + '\'' +
                ", estatus='" + estatus + '\'' +
                '}';
    }
}
