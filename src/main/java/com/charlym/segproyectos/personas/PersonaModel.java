package com.charlym.segproyectos.personas;

import javax.persistence.*;


@Entity(name  = "Personas")
@NamedNativeQuery(name="PeronasModel.findByEmpresa",
        query="select * from personas where empresa = ?1",
        resultClass=PersonaModel.class)
@NamedNativeQuery(name="PeronasModel.findByPerfil",
        query="select * from personas where perfil = ?1",
        resultClass=PersonaModel.class)
@NamedNativeQuery(name="PeronasModel.findByTipoContrato",
        query="select * from personas where tipo_contrato = ?1",
        resultClass=PersonaModel.class)
@NamedNativeQuery(name="PeronasModel.findByFechaIngreso",
        query="select * from personas where date(fecha_ingreso) = date(?1)",
        resultClass=PersonaModel.class)
public class PersonaModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nombre;
    private String apellidoPat;
    private String apellidoMat;
    private String fechaIngreso;
    private int tipoContrato;
    private String empresa;
    private int perfil;

    public PersonaModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPat() {
        return apellidoPat;
    }

    public void setApellidoPat(String apellidoPat) {
        this.apellidoPat = apellidoPat;
    }

    public String getApellidoMat() {
        return apellidoMat;
    }

    public void setApellidoMat(String apellidoMat) {
        this.apellidoMat = apellidoMat;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public int getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(int tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public int getPerfil() {
        return perfil;
    }

    public void setPerfil(int perfil) {
        this.perfil = perfil;
    }

    @Override
    public String toString() {
        return "PersonaModel{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", apellidoPat='" + apellidoPat + '\'' +
                ", apellidoMat='" + apellidoMat + '\'' +
                ", fechaIngreso='" + fechaIngreso + '\'' +
                ", tipoContrato='" + tipoContrato + '\'' +
                ", empresa='" + empresa + '\'' +
                ", perfil=" + perfil +
                '}';
    }
}
