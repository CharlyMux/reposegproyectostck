package com.charlym.segproyectos.personas;

import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface PersonasRepository extends CrudRepository<PersonaModel,Integer> {
    //List<PersonaModel> findAllIn();
    List<PersonaModel> findByEmpresa(String empresa);
    List<PersonaModel> findByPerfil(int perfil);
    List<PersonaModel> findByFechaIngreso(String fechaIngreso);
    List<PersonaModel> findByTipoContrato(int tipoContrato);

}