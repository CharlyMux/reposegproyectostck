package com.charlym.segproyectos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SegproyectoscmmApplication {

	public static void main(String[] args) {
		SpringApplication.run(SegproyectoscmmApplication.class, args);
	}

}
